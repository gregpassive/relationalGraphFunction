//
//  ViewController.swift
//  relationalGraphFunction
//
//  Created by Greg Hughes on 2/10/22.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        
        
//
        // Do any additional setup after loading the view.
        

        //        Here is the C code for the pre-order traversal
//    https://gitlab.com/PassiveLogic/firmware/sdk-pl/-/blob/AHR-application/subsys/bluetooth/passive_mesh/network/root_node/logging/net_topology/network_root_log_net_topology.c

        
        
//        preOrder traversal in order to name all the nodes
        
let graph = setupGraph()
        mainFunction(netDepth: Node<Int>.getDepth(nextNode: graph), netBranches: Node<Int>.getMaxWidth(nextNode: graph), nodeIndex: 6)
    }
    
    
}


class Node<Value> {
    
    internal init(value: Value) {
        self.value = value
    }

    var parent: Node? = nil
    var children: [Node] = []
    var value: Value
    
    func add(child: Node) {
        
        children.append(child)
        child.parent = self
    }
    
    

    static func getDepth(nextNode: Node) -> Int {
        var result = 0
        for node in nextNode.children {
            
            result = max(result, getDepth(nextNode: node) + 1)
        }
        
        return result
    }

 static func getMaxWidth(nextNode: Node) -> Int{
        var width = nextNode.children.count
        
        for child in nextNode.children {
          width = max(width, getMaxWidth(nextNode: child))
        }
        
       return width
    }
}




                /*
              The Graph looks like this
                 
                        root
                      /      \
            nodeParent1     nodeParent2
                    /           \
        node3ChildOf1           node4ChildOf1
                 /                 \
    node9ChildOf3                   node12ChildOf4
                 
                 
                 */
 


func setupGraph() -> Node<Int> {
    let root = Node(value: 0)

    let nodeParent1 = Node(value: 1)
    root.add(child: nodeParent1)
    
    let nodeParent2 = Node(value: 2)
    root.add(child: nodeParent2)
    
    let node3ChildOf1 = Node(value: 3)
    nodeParent1.add(child: node3ChildOf1)
    let node4ChildOf1 = Node(value: 4)
    nodeParent1.add(child: node4ChildOf1)
    let node5ChildOf1 = Node(value: 5)
    nodeParent1.add(child: node5ChildOf1)

    let node6ChildOf2 = Node(value: 6)
    nodeParent2.add(child: node6ChildOf2)
    
    let node7ChildOf2 = Node(value: 7)
    nodeParent2.add(child: node7ChildOf2)
    
    let node8ChildOf2 = Node(value: 8)
    nodeParent2.add(child: node8ChildOf2)
    
    let node9ChildOf3 = Node(value: 9)
    node3ChildOf1.add(child: node9ChildOf3)
    let node10ChildOf3 = Node(value: 10)
    node3ChildOf1.add(child: node10ChildOf3)
    let node11ChildOf3 = Node(value: 11)
    node3ChildOf1.add(child: node11ChildOf3)

    let node12ChildOf4 = Node(value: 12)
    node4ChildOf1.add(child: node12ChildOf4)
   
    
    print("the Depth ", Node.getDepth(nextNode: root))
    print("max Width ", Node.getMaxWidth(nextNode: root))
    return root
}

var meshNetDepth = 0
var meshNetBranches = 0
var meshNodeIndex = 0
var meshNumIndices = 0
var indexRootNode = 0

func getIndexCount(branches: Int, depth: Int) -> Int {
//    let theDepth = depth
    
    if branches == 1 {
        return branches * (depth + 1)
    }
    
    let doubleDepth = Double(depth)
    let doubleBranches = Double(branches)
    
    return Int((pow(doubleBranches, doubleDepth + 1) - 1) / (doubleBranches - 1))
    
}

func getNodeLayer(nodeIndex: Int) -> Int{
    var nodeLayer = 0
    var theNodeIndex = nodeIndex
    while nodeLayer < meshNetDepth {
        
        let subtreeVertexCount = getIndexCount(branches: meshNetBranches, depth: meshNetDepth - nodeLayer)
        let subtreeFirstLayerBranchIdx = Int(theNodeIndex / subtreeVertexCount)
        
        if theNodeIndex % subtreeVertexCount == 0 {
            break
        }
        
        nodeLayer += 1
        theNodeIndex -= subtreeVertexCount * subtreeFirstLayerBranchIdx + 1
    }
    return nodeLayer
}

func getChildIndex(nodeIndex: Int, childSubIndex: Int) -> Int {
    
    let nodeLayer = getNodeLayer(nodeIndex: nodeIndex)
    
    let indexCount =  getIndexCount(branches: meshNetBranches, depth: meshNetDepth - nodeLayer - 1)
    return nodeIndex + 1 + (childSubIndex * indexCount)
}



func getParentIndex(nodeIndex: Int) -> Int {
    
    let nodeLayer = getNodeLayer(nodeIndex: nodeIndex)
    
    var i = nodeIndex - 1
    
    while i >= 0 {
        let layer = getNodeLayer(nodeIndex: i)
        if layer < nodeLayer {
            return i
        }
        i -= 1
    }
    return 0
}

func getNodesPerBranch(nodeLayer: Int) -> Int{
    //Get relative depth
    let relativeDepth = meshNetDepth - nodeLayer
    //Get number of nodes including ourself
    var v = getIndexCount(branches: meshNetBranches, depth: relativeDepth)
    //Remove ourselves from the count
    v -= 1
    //Divide total count by branches to get nodes per branch
    return Int(v/meshNetBranches)
}

func getBranchIndex(nodeIndex: Int, childIndex: Int) -> Int {
    print("nodeIndex ",nodeIndex)
    print("childIndex ",childIndex)
//    # Get node layer
    let nodeLayer = getNodeLayer(nodeIndex: nodeIndex)
//    # See how many nodes are above us
    let relativeDepth = meshNetDepth - nodeLayer
    let numNodes = getIndexCount(branches: meshNetBranches, depth: relativeDepth)
    
//    # Make sure child index is within range
    if childIndex > nodeIndex && childIndex <= (nodeIndex + numNodes - 1) {
//        # See which branch connects
        let numNodesPerBranch = getNodesPerBranch(nodeLayer: nodeLayer)
        
        let branchRangeStartIndex = nodeIndex
        
        for i in 0..<meshNetBranches {
            let branchRangeEndIndex = branchRangeStartIndex + ((i + 1) * numNodesPerBranch)
            if childIndex > branchRangeStartIndex && childIndex <= branchRangeEndIndex {
                return i
            }
        }
    }
    print("HIT")
    return -1
}
//# Get node layer
func getNumberOfChildren(nodeIndex: Int) -> Int{
    let nodeLayer = getNodeLayer(nodeIndex: nodeIndex)
    if nodeLayer == meshNetDepth {
        return 0
    } else {
        return meshNetBranches
    }
}
func sourceFilePrintNodeChildren(nodeIndex: Int) {
    
    print("child_indices = ")
    for child in 0..<meshNetBranches {
        //file write
        print("child_indices = ", getChildIndex(nodeIndex: nodeIndex, childSubIndex: child))
    }
}
func sourceFilePrintRootNode() {
    
    print("parent Index = -1")
    print("node sub index -1")
    print("num children = \(meshNetBranches)")
    
    sourceFilePrintNodeChildren(nodeIndex: indexRootNode)
    
}
func sourceFilePrintNode(nodeIndex: Int) {
    let parentIndex = getParentIndex(nodeIndex: nodeIndex)
    let branchIndex = getBranchIndex(nodeIndex: parentIndex, childIndex: nodeIndex)
    let numberOfChildren = getNumberOfChildren(nodeIndex: nodeIndex)
    
    print("Parent index = \(parentIndex)")
    print( "node Sub Index = \(branchIndex)")
    print("num children = \(numberOfChildren)")
    if numberOfChildren != 0 {
        sourceFilePrintNodeChildren(nodeIndex: nodeIndex)
    }
    
}

func sourceFileGenerate() {
    print("sourceFileGenerate")
    if meshNodeIndex == indexRootNode {
        sourceFilePrintRootNode()
    } else {
        sourceFilePrintNode(nodeIndex: meshNodeIndex)
    }
}
func mainFunction(netDepth: Int, netBranches: Int, nodeIndex: Int) {

    meshNetDepth = netDepth
    meshNetBranches = netBranches
    meshNodeIndex = nodeIndex
    // Calculate total number of nodes in network# Calculate total number of nodes in network
    if meshNetDepth < 0 || meshNetBranches < 0 {
        print("Invalid Input")
    }
    // Calculate total number of nodes in network# Calculate total number of nodes in network
    meshNumIndices = getIndexCount(branches: meshNetBranches, depth: meshNetDepth)
    print("Number of indices \(meshNumIndices)")
    sourceFileGenerate()
}
